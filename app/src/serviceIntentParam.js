const config = require('../../serverConfig');

function outputFormat(requiredParamOutputPhrase, context) {
    return {
        'fulfillmentText': 'API responded perfectly',
        'fulfillmentMessages': [{
            'text': {
                'text': [requiredParamOutputPhrase]
            }
        }],
        'source': '',
        'outputContexts': context
    };
}

exports.allParamPresent = function(params, context) {
    // check if both of hospital and city are not present
    // if one of them present then the param is valid
    // because service can be searched by hospital or city
    const selectedContext = context.filter(contextElement =>
        contextElement.name.includes(config.SERVICE_INTENT.toLowerCase()) || contextElement.name.includes(config.GIVEN_NAME));
    if (params['hospital'] === '' && params['city'] === '') {
        return outputFormat(config.ASK_FOR_HOSPITAL_OR_CITY_SERVICE_OUTPUT_PHRASE, selectedContext);
    } else if (params['city'] != '' && params['service'] == '' && params['hospital'] == '') {
        return outputFormat(config.ASK_FOR_SPECIFIC_SERVICE + params['city'], selectedContext);
    }
    else {
        console.log('else clause in allParamPresent');
        return config.PARAM_FULFILLED;
    }
}