const config = require('../../serverConfig');

function outputFormat(requiredParamOutputPhrase, context) {
    return {
        'fulfillmentText': 'API responded perfectly',
        'fulfillmentMessages': [{
            'text': {
                'text': [requiredParamOutputPhrase]
            }
        }],
        'source': '',
        'outputContexts': context
    };
}

exports.allParamPresent = function(text, context) {
    console.log('text type '+ typeof(text));
    // check if user provide the uniqueId
    if (text.indexOf('ref-') == -1) {
        const selectedContext = context.filter(contextElement =>
            contextElement.name.includes(config.QUEUE_STATUS_INTENT.toLowerCase()) || contextElement.name.includes(config.GIVEN_NAME));
        return outputFormat(config.ASK_FOR_UNIQUE_ID_OUTPUT_PHRASE, selectedContext);
    }
    else {
        console.log('else clause in allParamPresent');
        return config.PARAM_FULFILLED;
    }
}