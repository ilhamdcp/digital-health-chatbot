const config = require('../../serverConfig');

function outputFormat(requiredParamOutputPhrase, context) {
    return {
        'fulfillmentText': 'API responded perfectly',
        'fulfillmentMessages': [{
            'text': {
                'text': [requiredParamOutputPhrase]
            }
        }],
        'source': '',
        'outputContexts': context
    };
}

exports.allParamPresent = function(params, context) {
    // selectContext first to reduce code duplication in conditional clause
    // check for doctorName, registerDate, and time. If one of them is not present then ask user to provide it
    const selectedContext = context.filter(contextElement =>
        contextElement.name.includes(config.REGISTER_INTENT.toLowerCase()) || contextElement.name.includes(config.GIVEN_NAME));
    if (params['doctorName'] === '') {
        return outputFormat(config.ASK_FOR_DOCTOR_REGISTRATION_OUTPUT_PHRASE, selectedContext);
    } else if (params['registerDate'] === '') {
        return outputFormat(config.ASK_FOR_DATE_REGISTRATION_OUTPUT_PHRASE, selectedContext);
    } else if (params['time'] === '') {
        return outputFormat(config.ASK_FOR_TIME_REGISTRATION_OUTPUT_PHRASE, selectedContext);
    }
    else {
        console.log('else clause in allParamPresent');
        return config.PARAM_FULFILLED;
    }
}