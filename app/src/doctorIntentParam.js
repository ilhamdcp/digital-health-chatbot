const config = require('../../serverConfig');

function outputFormat(requiredParamOutputPhrase, context) {
    const responseObj = {
        'fulfillmentText': 'API responded perfectly',
        'fulfillmentMessages': [{
            'text': {
                'text': [requiredParamOutputPhrase]
            }
        }],
        'source': '',
        'outputContexts': context
    }
    return responseObj;

}

exports.allParamPresent = function(intentName, params, context) {
    // check whether the incoming intent is doctorIntent or doctorWithSpecializationIntent
    // then check for the required param for each respective intent
    if (intentName == config.DOCTOR_INTENT && params['hospital'] === '') {
        const selectedContext = context.filter(contextElement => contextElement.name.includes(config.DOCTOR_INTENT.toLowerCase()) || contextElement.name.includes(config.GIVEN_NAME));
        console.log('if clause in allParamPresent');
        return outputFormat(config.ASK_FOR_HOSPITAL_DOCTOR_OUTPUT_PHRASE+config.AVAILABLE_HOSPITAL_LIST_PHRASE, selectedContext);
    }
    else if (intentName == config.DOCTOR_WITH_SPECIALIZATION_INTENT && params['speciality'] === '') {
        console.log(JSON.stringify(params));
        const selectedContext = context.filter(contextElement => contextElement.name.includes(config.DOCTOR_WITH_SPECIALIZATION_INTENT.toLowerCase()) || contextElement.name.includes(config.GIVEN_NAME));
        console.log('if clause in allParamPresent');
        return outputFormat(config.ASK_FOR_DOCTOR_SPECIALITY_OUTPUT_PHRASE, selectedContext);
    }
    else {
        console.log('else clause in allParamPresent');
        return config.PARAM_FULFILLED;
    }
}