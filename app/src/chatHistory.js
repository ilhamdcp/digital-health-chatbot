const config = require('../../serverConfig');

exports.saveChat = function(payload, text, db) {
    if (payload.source == config.LINE_PLATFORM){
        db.collection(config.USER_COLLECTION_NAME).findOneAndUpdate({'accountId': payload.data.source.userId}, {$push: {
            'chat_history': text
        }});
    } else if (payload.source == config.TWILIO_PLATFORM){
        db.collection(config.USER_COLLECTION_NAME).findOneAndUpdate({'accountId': payload.data.From}, {$push: {
            'chat_history': text
        }});
    } else if (payload.source == config.TELEGRAM_PLATFORM){
        db.collection(config.USER_COLLECTION_NAME).findOneAndUpdate({'accountId': payload.data.message.from.username}, {$push: {
            'chat_history': text
        }});
    }

}