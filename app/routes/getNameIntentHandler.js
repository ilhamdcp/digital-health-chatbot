const config = require('../../serverConfig');

const outputFormat = (output, name, context) => {
    var responseObj;
    if (context) {
        // setup to the next context when the user is successfully registered
        responseObj = {
            'fulfillmentText': 'API responded perfectly',
            'fulfillmentMessages': [{
                'text': {
                    'text': [output]
                }
            }],
            'source': '',
            "outputContexts": [{
                "name": context,
                "lifespanCount": 9999,
                "parameters": {
                    "nama": name,
                    "nama.original": name
                }
            }]
        }
    } else {
        // back to the initial where there is no context
        responseObj = {
            'fulfillmentText': 'API responded perfectly',
            'fulfillmentMessages': [{
                'text': {
                    'text': [output]
                }
            }],
            'source': '',
        }
    }

    return responseObj;
}

module.exports = {
    register: (text, payload, session, db, res) => {
        // split sentence and get the name by slicing the name identifier, such as: saya
        var name = text.split(" ").slice(1).join(" ");
        if (name == '' || name == undefined) {
            return res.json(outputFormat(config.INVALID_NAME_OUTPUT_PHRASE, name, session + null));
        }
        // check the platform whether from LINE or twilio
        // can be modified if there is a new platform
        if (payload.source == config.LINE_PLATFORM) {
            return db.collection(config.USER_COLLECTION_NAME).insertOne({
                'name': name,
                'accountId': payload.data.source.userId,
                'source': config.LINE_PLATFORM,
                'chat_history': [text]
            }, function (err, insertedDoc) {
                // @param result: contains the data when the insertion is successful
                // the inserted data contained in the ops section
                const data = insertedDoc.ops[0];
                if (data) {
                    // only use the firstname to greet the user
                    const firstName = data.name.split(' ')[0];
                    return res.json(outputFormat('Hai ' + firstName + config.KEYWORD_INSTRUCTION_OUTPUT_PHRASE, firstName, session + '/contexts/given-name'));
                } else {
                    return res.json(outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE, null, null));
                }
            });
        } else if (payload.source == config.TWILIO_PLATFORM) {
            return db.collection(config.USER_COLLECTION_NAME).insertOne({
                'name': name,
                'accountId': payload.data.From,
                'source': config.TWILIO_PLATFORM,
                'chat_history': [text]
            }, function (err, result) {
                const data = result.ops[0];
                if (data) {
                    const firstName = data.name.split(' ')[0];
                    return res.json(outputFormat('Hai ' + firstName + config.KEYWORD_INSTRUCTION_OUTPUT_PHRASE, firstName, session + '/contexts/given-name'));
                } else {
                    return res.json(outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE, null, null));
                }
            });
        } else if (payload.source == config.TELEGRAM_PLATFORM) {
            return db.collection(config.USER_COLLECTION_NAME).insertOne({
                'name': name,
                'accountId': payload.data.message.from.id,
                'source': config.TELEGRAM_PLATFORM,
                'chat_history': [text]
            }, function (err, result) {
                const data = result.ops[0];
                if (data) {
                    const firstName = data.name.split(' ')[0];
                    return res.json(outputFormat('Hai ' + firstName + config.KEYWORD_INSTRUCTION_OUTPUT_PHRASE, firstName, session + '/contexts/given-name'));
                } else {
                    return res.json(outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE, null, null));
                }
            });
        }
    }
}