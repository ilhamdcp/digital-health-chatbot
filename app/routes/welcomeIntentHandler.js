const config = require('../../serverConfig');

const outputFormat = (output, name, context) => {
    var responseObj;
    if (name) {
        // when the name exists in the db, then skip the getNameIntent
        responseObj = {
            'fulfillmentText': 'API responded perfectly',
            'fulfillmentMessages': [{
                'text': {
                    'text': [output]
                }
            }],
            'source': '',
            "outputContexts": [
                {
                  "name": context,
                  "lifespanCount": 9999,
                  "parameters": {
                    "nama": name,
                    "nama.original": name
                  }
                }
              ]
        }
    } else {
        responseObj = {
            // when doesn't exist, go to the getNameIntent
            'fulfillmentText': 'API responded perfectly',
            'fulfillmentMessages': [{
                'text': {
                    'text': [output]
                }
            }],
            'source': '',
            "outputContexts": [{
                "name": context,
                "lifespanCount": 5,
                "parameters": {}
            }],
        }
    }

    return responseObj;
}

module.exports = {
    searchName: (payload, session, db, res) => {
        // find the document inside the collection 'user'
        // if exist then greet with the firstname
        // if not then ask the user to register
        if (payload.source) {
            // check if whether user is using LINE or Whatsapp
            if (payload.source == config.LINE_PLATFORM) {
                // find user in db
                return db.collection(config.USER_COLLECTION_NAME).findOne({
                    'accountId': payload.data.source.userId,
                    'source': config.LINE_PLATFORM
                }, function (error, userDoc) {
                    // if user is exist, then greet with the first name
                    if (userDoc) {
                        const firstName = userDoc.name.split(' ')[0];
                        return res.json(outputFormat('Hai ' + firstName + ', ada yang bisa saya bantu?', firstName, session + config.GIVEN_NAME_CONTEXT));
                    } 
                    // if the user isn't exist, ask the user to redo with greeting
                    else {
                        return res.json(outputFormat(config.GREET_AND_NAME_FORMAT_OUTPUT_PHRASE, null, session + config.USER_EXIST_CONTEXT));
                    }
                });
            } else if (payload.source == config.TWILIO_PLATFORM) {
                return db.collection(config.USER_COLLECTION_NAME).findOne({
                    'accountId': payload.data.From,
                    'source': config.TWILIO_PLATFORM
                }, function (error, userDoc) {
                    if (userDoc) {
                        const firstName = userDoc.name.split(' ')[0];
                        return res.json(outputFormat('Hai ' + firstName + ', ada yang bisa saya bantu?', firstName, session + config.GIVEN_NAME_CONTEXT));
                    } else {
                        return res.json(outputFormat(config.GREET_AND_NAME_FORMAT_OUTPUT_PHRASE, null, session + config.USER_EXIST_CONTEXT));
                    }
                });
            }
            else if (payload.source == config.TELEGRAM_PLATFORM) {
                return db.collection(config.USER_COLLECTION_NAME).findOne({
                    'accountId': payload.data.message.from.id,
                    'source': config.TELEGRAM_PLATFORM
                }, function (error, userDoc) {
                    if (userDoc) {
                        const firstName = userDoc.name.split(' ')[0];
                        return res.json(outputFormat('Hai ' + firstName + ', ada yang bisa saya bantu?', firstName, session + config.GIVEN_NAME_CONTEXT));
                    } else {
                        return res.json(outputFormat(config.GREET_AND_NAME_FORMAT_OUTPUT_PHRASE, null, session + config.USER_EXIST_CONTEXT));
                    }
                });
            }
        }
    }
}