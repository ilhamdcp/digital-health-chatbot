const config = require('./../../serverConfig');
const axios = require('axios');

// check the queueStatus
const queueStatus = (uniqueId, context) => {
    return axios.get(config.QUEUE_API_URL + '?uniqueId=' + uniqueId).then(response => {
            // if the user has already booked
            if (response.data != {}) {
                return 'Nomor antrian kamu untuk dr. ' + response.data.doctorName + ' adalah ' + response.data.specificQueue + ' dan saat ini status antriannya adalah ' + response.data.status;
            } 
            // if hasn't
            else {
                return config.INVALID_ID_OUTPUT_PHRASE;
            }
        })
        .then(
            output => {
                return outputFormat(output, context);
            }
        )        
        .catch(error => {
            console.log(error);
            return outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE);
        });
}
// output formatting for dialogflow
const outputFormat = (output, context) => {
    const responseObj = {
        'fulfillmentText': 'API responded perfectly',
        'fulfillmentMessages': [{
            'text': {
                'text': [output]
            }
        }],
        'source': '',
        outputContexts: context
    }
    return responseObj;
}

const findKey = (sentence) => {
    return sentence.split(' ').filter(text => text.startsWith('ref-')).pop();
}

// used on server.js
module.exports = {
    viewQueue: (requestParam, context) => {
        if (requestParam.queryText) {
            const selectedContext = context.filter(contextElement => contextElement.name.includes(config.GIVEN_NAME));
            selectedContext[0].parameters = {};
            const key = findKey(requestParam.queryText);
            return queueStatus(key, selectedContext);
        } else {
            return outputFormat(config.INVALID_ID_OUTPUT_PHRASE, context);
        }
    }
}
