const axios = require('axios');
const config = require('../../serverConfig');

var outputText;

// call API with axios
// note the usage of 'then' to handle promise
// to execute following command when current promise are already received


const findSpecific = (hospitalName, serviceName, cityName, context) => {
  const initialPhrase = "Rumah sakit " + hospitalName + " yang berada di " + cityName + " memiliki layanan " + serviceName;
  const unavailableInitialPhrase = "Rumah sakit " + hospitalName + " yang berada di " + cityName + " tidak memiliki layanan tersebut";
  return axios.get(config.SERVICE_API_URL + '?name=' + hospitalName + '&facilities=' + serviceName + '&city=' + cityName)
    .then(response => {

      if (response.data.length > 0) {
        outputText = initialPhrase;
      } else {
        outputText = unavailableInitialPhrase;
      }
      return outputText
    })
    .then(
      output => {
        return outputFormat(output, context);
      }
    )
    .catch(error => {
      console.log(error);
      return outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE);
    });
}

const findByHospital = (hospitalName, context) => {
  const initialPhrase = config.SERVICE_LIST_OUTPUT_PHRASE;

  return axios.get(config.SERVICE_API_URL + '?name=' + hospitalName)
    .then(response => {

      if (response.data.length > 0) {
        outputText = response.data.reduce((acc, filtered) =>
          acc + '\n\n' + filtered.name + ': ' + filtered.facilities.reduce(
            (acc, facilitiesName) => acc + '\n- ' + facilitiesName, ''), initialPhrase);
      } else {
        outputText = config.UNAVAILABLE_HOSPITAL_OUTPUT_PHRASE;
      }
      return outputText
    })
    .then(
      output => {
        return outputFormat(output, context);
      }
    )
    .catch(error => {
      console.log(error);
      return outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE);
    });
}

const findByHospitalNameAndService = (hospital, service, context) => {

  return axios.get(config.SERVICE_API_URL + '?name=' + hospital + "&facilities=" + service)
    .then(response => {
      console.log('length: ' + response.data);
      if (response.data.length > 0) {
        outputText = config.AVAILABLE_SERVICE_IN_HOSPITAL_OUTPUT_PHRASE + service;
      } else {
        console.log(response.data);
        outputText = config.UNAVALIABLE_SERVICE_IN_HOSPITAL_OUTPUT_PHRASE + service;
      }
      return outputText
    })
    .then(
      output => {
        return outputFormat(output, context);
      }
    )
    .catch(error => {
      console.log(error);
      return outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE);
    });
}

const findByCityAndService = (city, facilities, context) => {
  const initialPhrase = config.HOSPITAL_IN_CITY_OUTPUT_PHRASE + city + ':\n';

  return axios.get(config.SERVICE_API_URL + '?city=' + city + "&facilities=" + facilities)
    .then(response => {
      if (response.data != []) {
        outputText = response.data.reduce((acc, filtered) =>
          acc + '\n' + filtered.name, initialPhrase);
      } else {
        outputText = config.UNVAILABLE_SERVICE_IN_ALL_HOSPITAL_OUTPUT_PHRASE;
      }
      return outputText
    })
    .then(
      output => {
        return outputFormat(output, context);
      }
    )
    .catch(error => {
      console.log(error);
      return outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE);
    });
}

const findByService = (facilities, context) => {
  const initialPhrase = config.SERVICE_LIST_OUTPUT_PHRASE;

  return axios.get(+"?facilities=" + facilities)
    .then(response => {
      if (response.data.length > 0) {
        outputText = response.data.reduce((acc, filtered) =>
          acc + '\n- ' + filtered.name + ' di ' + filtered.city, initialPhrase);
      } else {
        outputText = config.UNVAILABLE_SERVICE_IN_ALL_HOSPITAL_OUTPUT_PHRASE;
      }
      return outputText
    })
    .then(
      output => {
        return outputFormat(output, context);
      }
    )
    .catch(error => {
      console.log(error);
      return outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE);
    });
}

// output formatting for dialogflow
const outputFormat = (output, context) => {
  const responseObj = {
    'fulfillmentText': 'API responded perfectly',
    'fulfillmentMessages': [{
      'text': {
        'text': [output]
      }
    }],
    'source': '',
    'outputContexts': context
  }
  return responseObj;
}

// used on server.js
module.exports = {
  searchService: (requestParam, context) => {
    // flush context parameters
    const selectedContext = context.filter(contextElement => contextElement.name.includes(config.GIVEN_NAME))
    selectedContext[0].parameters.hospital = '';
    selectedContext[0].parameters['hospital.original'] = '';
    console.log('serviceIntent selected context => ' + JSON.stringify(selectedContext));
    // check for the param available to request the API with the appropriate url
    if (requestParam.hospital && requestParam.service && requestParam.city) {
      return findSpecific(requestParam.hospital, requestParam.service, requestParam.city, selectedContext);
    } else if (requestParam.hospital && requestParam.service) {
      return findByHospitalNameAndService(requestParam.hospital, requestParam.service, selectedContext);
    } else if (requestParam.city && requestParam.service) {
      return findByCityAndService(requestParam.city, requestParam.service, selectedContext);
    } else if (requestParam.hospital) {
      return findByHospital(requestParam.hospital, selectedContext);
    } else if (requestParam.service) {
      return findByService(requestParam.service, selectedContext);
    } else {
      return outputFormat(config.INVALID_INPUT_OUTPUT_PHRASE);
    }
  }
}