const outputFormat = (output, context) => {
    responseObj = {
        'fulfillmentText': 'API responded perfectly',
        'fulfillmentMessages': [{
            'text': {
                'text': [output]
            }
        }],
        'source': '',
        "outputContexts": context
    }
    return responseObj;
}

module.exports = {
    // this module is for cancelIntent, to reset the context to given-name
    reset: (output, context) => {
        const selectedContext = context.filter(contextElement => contextElement.name.includes('given-name'));
        selectedContext[0].parameters = {};
        return outputFormat(output, selectedContext)
    }
}