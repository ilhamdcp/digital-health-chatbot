const config = require('../../serverConfig');
const axios = require('axios');

var outputText;

// call API with axios
// note the usage of then to handle promise
// to execute following command when current promise are already received
const findByHospital = (hospitalName, context) => {
    const initialPhrase = config.DOCTOR_LIST_IN_HOSPITAL_OUTPUT_PHRASE + hospitalName + ' adalah:'

    return axios.get(config.DOCTOR_API_URL + '?rs=' + hospitalName)
        .then(response => {
            if (response.data.length > 0) {
                outputText = response.data.reduce((acc, filtered) =>
                    acc + '\n' + filtered.name + ' (' + filtered.speciality + ') jadwal praktik: ' + filtered.schedule, initialPhrase);
            } else {
                outputText = config.UNAVAILABLE_DOCTOR_OUTPUT_PHRASE;
            }
            return outputText
        })
        .then(
            output => {
                return outputFormat(output, context);
            }
        )
        .catch(error => {
            console.log(error);
            return outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE);
        });
}

const findByHospitalAndName = (hospital, name, context) => {
    const initialPhrase = config.DOCTOR_SPECIALIST_IN_HOSPITAL_OUTPUT_PHRASE + hospital + ':';

    return axios.get(config.DOCTOR_API_URL + '?rs=' + hospital + "&name=" + name)
        .then(response => {
            if (response.data.length > 0) {
                outputText = response.data.reduce((acc, filtered) =>
                    acc + '\n' + filtered.name + ' (' + filtered.speciality + ') jadwal praktik: ' + filtered.schedule, initialPhrase);
            } else {
                outputText = config.UNAVAILABLE_DOCTOR_OUTPUT_PHRASE;
            }
            return outputText
        })
        .then(
            output => {
                return outputFormat(output, context);
            }
        )
        .catch(error => {
            console.log(error);
            return outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE);
        });
}

const findByHospitalAndSpeciality = (hospital, speciality, context) => {
    const initialPhrase = config.DOCTOR_SPECIALIST_IN_HOSPITAL_OUTPUT_PHRASE + hospital + ':';

    return axios.get(config.DOCTOR_API_URL + '?rs=' + hospital + "&speciality=" + speciality)
        .then(response => {
            if (response.data.length > 0) {
                outputText = response.data.reduce((acc, filtered) =>
                    acc + '\n' + filtered.name + ' (' + filtered.speciality + ') jadwal praktik: ' + filtered.schedule, initialPhrase);
            } else {
                outputText = config.UNAVAILABLE_DOCTOR_OUTPUT_PHRASE;
            }
            return outputText
        })
        .then(
            output => {
                return outputFormat(output, context);
            }
        )
        .catch(error => {
            console.log(error);
            return outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE);
        });
}

const findBySpeciality = (speciality, context) => {
    const initialPhrase = config.SPECIALIST_DOCTOR_OUTPUT_PHRASE;

    return axios.get(config.DOCTOR_API_URL + '?speciality=' + speciality)
        .then(response => {
            if (response.data.length > 0) {
                outputText = response.data.reduce((acc, filtered) =>
                    acc + '\n' + filtered.name + ' (' + filtered.speciality + ') di ' + filtered.hospital, initialPhrase);
            } else {
                outputText = config.UNAVAILABLE_DOCTOR_OUTPUT_PHRASE;
            }
            return outputText
        })
        .then(
            output => {
                return outputFormat(output, context);
            }
        )
        .catch(error => {
            console.log(error);
            return outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE);
        });
}

// output formatting for dialogflow
const outputFormat = (output, context) => {
    const responseObj = {
        'fulfillmentText': 'API responded perfectly',
        'fulfillmentMessages': [{
            'text': {
                'text': [output]
            }
        }],
        'source': '',
        'outputContexts': context
    }
    return responseObj;
}

// used on server.js
module.exports = {
    searchDoctor: (requestParam, context) => {
        // flush context parameters
        const selectedContext = context.filter(contextElement => contextElement.name.includes(config.GIVEN_NAME));
        selectedContext[0].parameters.hospital = '';
        selectedContext[0].parameters.hospital.original = '';

        console.log('doctorIntent selected context => ' + JSON.stringify(selectedContext));
        if (requestParam.hospital && requestParam.name) {
            return findByHospitalAndName(requestParam.hospital, requestParam.name, selectedContext);
        } else if (requestParam.hospital && requestParam.speciality) {
            return findByHospitalAndSpeciality(requestParam.hospital, requestParam.speciality, selectedContext);
        } else if (requestParam.hospital) {
            return findByHospital(requestParam.hospital, selectedContext);
        } else if (requestParam.speciality) {
            return findBySpeciality(requestParam.speciality, selectedContext);
        } else {
            return outputFormat(config.INVALID_INPUT_OUTPUT_PHRASE);
        }
    }
}