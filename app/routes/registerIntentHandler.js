const config = require('../../serverConfig')
const axios = require('axios');
const dateFormat = require('dateformat');

// register to db
const registerToDatabase = (deviceId, doctorName, date, time, context) => {
    var outputText;
    return axios.post(config.REGISTER_API_URL + '?deviceId=' + deviceId + '&doctorName=' + doctorName + '&date=' + date + '&time=' + time)
        .then(response => {
            // check if user hasn't booked with the same doctor
            if (response.data != false) {
                outputText = config.REGISTRATION_DONE_OUTPUT_PHRASE_1 + response.data + config.REGISTRATION_DONE_OUTPUT_PHRASE_2;
            } else {
                outputText = config.USER_ALREADY_EXIST_OUTPUT_PHRASE_1 + doctorName + config.USER_ALREADY_EXIST_OUTPUT_PHRASE_2;
            }
            return outputText
        })
        .then(
            output => {
                return outputFormat(output, context);
            }
        )
        .catch(error => {
            console.log(error);
            return outputFormat(config.SERVER_ERROR_OUTPUT_PHRASE);
        });
}

// output formatting for dialogflow
const outputFormat = (output, context) => {
    const responseObj = {
        fulfillmentText: 'API responded perfectly',
        fulfillmentMessages: [{
            'text': {
                'text': [output]
            }
        }],
        'source': '',
        outputContexts: context
    }
    return responseObj;
}

// used on server.js
module.exports = {
    consultRegistration: (requestParam, payload, context) => {
        const selectedContext = context.filter(contextElement => contextElement.name.includes(config.GIVEN_NAME));
        selectedContext[0].parameters = {};
        if (payload.source == config.LINE_PLATFORM) {
            return registerToDatabase(payload.data.source.userId, requestParam.doctorName, dateFormat(requestParam.registerDate, 'dd-mm-yyyy'), dateFormat(requestParam.time, 'HH:MM'), selectedContext);
        } else if (payload.source == config.TWILIO_PLATFORM) {
            return registerToDatabase(payload.data.From, requestParam.doctorName, dateFormat(requestParam.registerDate, 'dd-mm-yyyy'), dateFormat(requestParam.time, 'HH:MM'), selectedContext);
        } else if (payload.source == config.TELEGRAM_PLATFORM) {
            return registerToDatabase(payload.data.message.from.username, requestParam.doctorName, dateFormat(requestParam.registerDate, 'dd-mm-yyyy'), dateFormat(requestParam.time, 'HH:MM'), selectedContext);
        }
    }
}
