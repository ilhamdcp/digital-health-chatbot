const express = require('express');
const bodyParser = require('body-parser');
const axios = require ('axios');
const app = express();
const port = process.env.PORT || 8000;
const url = 'http://search-doctor.herokuapp.com/doctor'



app.use(bodyParser.json());
app.get('/', function(req, res) {
    return (axios.get(url + '?rs=RSUI')
    .then(response => {
        console.log(response.data);
        return ({
        'fulfillmentText': 'API responded perfectly',
        'fulfillmentMessages': [{
            'text': {
                'text': response.data
            }
        }],
        'source': ''
        });
    })
    .catch(error => {
        console.log(error);
    })).then(result => res.send(result));
});


app.listen(port, () => {
    console.log('We are live on ' + port);
});