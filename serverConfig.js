module.exports = Object.freeze({
    UNDEFINED: undefined,
    WELCOME_INTENT: "welcomeIntent",
    GET_NAME_INTENT: "getNameIntent",
    DOCTOR_INTENT: "doctorIntent",
    DOCTOR_WITH_SPECIALIZATION_INTENT: "doctorWithSpecializationIntent",
    SERVICE_INTENT: "serviceIntent",
    QUEUE_STATUS_INTENT: "queueStatusIntent",
    REGISTER_INTENT: "registerIntent",
    CANCEL_INTENT: "cancelIntent",
    DB_NAME: "digital-health-chatbot",
    DOCTOR_INTENT_HANDLER_DIRECTORY:  "./app/routes/doctorIntentHandler",
    SERVICE_INTENT_HANDLER_DIRECTORY: "./app/routes/serviceIntentHandler",
    REGISTER_INTENT_HANDLER_DIRECTORY: "./app/routes/registerIntentHandler",
    QUEUE_STATUS_INTENT_HANDLER_DIRECTORY: "./app/routes/queueStatusIntentHandler",
    WELCOME_INTENT_HANDLER_DIRECTORY: "./app/routes/welcomeIntentHandler",
    GET_NAME_INTENT_HANDLER_DIRECTORY: "./app/routes/getNameIntentHandler",
    CANCEL_INTENT_HANDLER_DIRECTORY: "./app/routes/cancelIntentHandler",
    CHAT_HISTORY_DIRECTORY: "./app/src/chatHistory",
    DOCTOR_INTENT_PARAM_DIRECTORY: "./app/src/doctorIntentParam",
    SERVICE_INTENT_PARAM_DIRECTORY: "./app/src/serviceIntentParam",
    QUEUE_STATUS_INTENT_PARAM_DIRECTORY: "./app/src/queueStatusIntentParam",
    REGISTER_INTENT_PARAM_DIRECTORY: "./app/src/registerIntentParam",
    CANCEL_INTENT_OUTPUT_PHRASE: "Baiklah, kamu bisa kembali lagi untuk mencari dokter, mencari layanan untuk rumah sakit tertentu, memesan konsultasi, dan untuk mengecek nomor antrian :)",

    // url
    DB_URL: "mongodb://admin:admin1234@ds253468.mlab.com:53468/digital-health-chatbot",


    // doctorIntent config
    DOCTOR_API_URL: "http://search-doctor.herokuapp.com/doctor",

    UNAVAILABLE_DOCTOR_OUTPUT_PHRASE: "Maaf, dokter yang kamu cari tidak ada:(",
    SERVER_ERROR_OUTPUT_PHRASE: "Mohon maaf, terdapat kesalahan pada server kami",
    SPECIALIST_DOCTOR_OUTPUT_PHRASE: "Berikut ini dokter spesialis yang kamu cari:",
    INVALID_INPUT_OUTPUT_PHRASE: "Mohon maaf, input kamu tidak valid",
    DOCTOR_SPECIALIST_IN_HOSPITAL_OUTPUT_PHRASE: "Mungkin ini dokter yang kamu maksud di ",
    DOCTOR_LIST_IN_HOSPITAL_OUTPUT_PHRASE: "Dokter yang praktik di ",


    //getNameIntent config
    LINE_PLATFORM: "line",
    USER_COLLECTION_NAME: "user",
    TWILIO_PLATFORM: "twilio",
    TELEGRAM_PLATFORM: "telegram",

    KEYWORD_INSTRUCTION_OUTPUT_PHRASE: ", kamu sudah sukses terdaftar. Berikut ini daftar keyword yang bisa kamu gunakan:\n1. 'cari dokter' untuk mencari dokter\n2. 'layanan' untuk mencari layanan di rs tertentu\n3. 'antrian' untuk mengetahui kamu antrian keberapa\n4. 'request konsultasi' untuk melakukan konsultasi dengan dokter yang kamu inginkan",
    SERVER_ERROR_OUTPUT_PHRASE: "Wah ada kendala pada server kami sepertinya, mohon diulangi kembali ya",
    INVALID_NAME_OUTPUT_PHRASE: "Wah namanya tidak jelas nih",

    // queueStatusIntent config
    QUEUE_API_URL: "http://queue-api-chatbot.herokuapp.com/queue",
    INVALID_ID_OUTPUT_PHRASE: "Mohon maaf, id tersebut tidak valid",
    SERVER_ERROR_OUTPUT_PHRASE: "Mohon maaf, terdapat kesalahan pada server kami",


    // registerIntent config
    REGISTER_API_URL: "http://registration-api-chatbot.herokuapp.com/register",
    REGISTRATION_DONE_OUTPUT_PHRASE_1: "Proses registrasi kamu telah selesai, nomor id unik kamu adalah ",
    REGISTRATION_DONE_OUTPUT_PHRASE_2: ". Simpan nomor id tersebut untuk mengecek nomor antrian kamu",
    USER_ALREADY_EXIST_OUTPUT_PHRASE_1: "Mohon maaf, kamu sebelumnya sedang mengajukan konsultasi dengan dokter ",
    USER_ALREADY_EXIST_OUTPUT_PHRASE_2: ", silakan masukkan nomor id unik untuk mengecek nomor antrian kamu",
    SERVER_ERROR_OUTPUT_PHRASE: "Mohon maaf, terdapat kesalahan pada server kami",


    
    // serviceIntent config
    SERVICE_API_URL: "http://search-service.herokuapp.com/service",

    INVALID_INPUT_OUTPUT_PHRASE: "Mohon maaf, input kamu tidak valid",
    SERVER_ERROR_OUTPUT_PHRASE: "Mohon maaf, terdapat kesalahan pada server kami",
    SERVICE_LIST_OUTPUT_PHRASE: "Berikut ini daftar layanan yang tersedia pada rumah sakit yang kamu maksud:",
    HOSPITAL_IN_CITY_OUTPUT_PHRASE: "Berikut ini daftar rumah sakit yang memiliki layanan tersebut di kota ",
    AVAILABLE_SERVICE_IN_HOSPITAL_OUTPUT_PHRASE: "Rumah sakit yang kamu maksud memiliki layanan ",
    UNAVALIABLE_SERVICE_IN_HOSPITAL_OUTPUT_PHRASE: "Maaf, rumah sakit tersebut tidak memiliki layanan ",
    UNVAILABLE_SERVICE_IN_ALL_HOSPITAL_OUTPUT_PHRASE: "Maaf, tidak ada rumah sakit yang memiliki layanan tersebut",
    UNAVAILABLE_HOSPITAL_OUTPUT_PHRASE: "Maaf, rumah sakit yang kamu cari tidak ada:(",


    GREET_AND_NAME_FORMAT_OUTPUT_PHRASE: "Selamat datang di DoctoBot! Biar lebih akrab, yuk kenalan dulu, siapa nama panggilan kamu? Eits tapi ada aturannya nih, di depannya ada 'saya'. Contohnya: saya DoctoBot",
    GIVEN_NAME_CONTEXT: "/contexts/given-name",
    USER_EXIST_CONTEXT: "/contexts/namauser",


    // intentParam config
    PARAM_FULFILLED: "fulfilled",
    ASK_FOR_HOSPITAL_OR_CITY_SERVICE_OUTPUT_PHRASE: "Silakan cantumkan layanan pada kota mana atau rumah sakit apa yang ingin ditanyakan",
    ASK_FOR_SPECIFIC_SERVICE: "Silakan masukkan jenis layanan yang ingin kamu cari pada rumah sakit di kota ",
    ASK_FOR_HOSPITAL_DOCTOR_OUTPUT_PHRASE: "Kamu mau cari dokter di rumah sakit apa?",
    ASK_FOR_DOCTOR_SPECIALITY_OUTPUT_PHRASE: "Dokter spesialis apa yang kamu mau cari?",
    ASK_FOR_UNIQUE_ID_OUTPUT_PHRASE: "Silakan masukkan nomor id unik kamu",
    ASK_FOR_DOCTOR_REGISTRATION_OUTPUT_PHRASE: "Siapa nama dokter yang ingin kamu ajukan konsultasi?",
    ASK_FOR_DATE_REGISTRATION_OUTPUT_PHRASE: "Pada tanggal berapa kamu ingin mengajukan konsultasi?",
    ASK_FOR_TIME_REGISTRATION_OUTPUT_PHRASE: "Pada jam berapa kamu ingin mengajukan konsultasi?",

    AVAILABLE_HOSPITAL_LIST_PHRASE: "\nBerikut list rumah sakit yang tersedia: " +
        "\n1. RSUI" +
        "\n2. RSPP" +
        "\n3. RSCM" +
        "\n4. Mayapada" +
        "\n5. Asih",


    GIVEN_NAME: "given-name",
    ID_DIALOG_CONTEXT: "id_dialog_context"


});