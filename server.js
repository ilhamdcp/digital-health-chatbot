// importing from different files and libraries
// 1. doctor, service, reg_and_queue, user, and new_user are required files to be used in this server
// 2. express: library for url routing
// 3. bodyParser: parse incoming request bodies in a middleware before your handlers, available under the req.body property
// 4. config: contains constants to reduce hardcode

const config = require('./serverConfig');
const doctorIntentHandler = require(config.DOCTOR_INTENT_HANDLER_DIRECTORY);
const serviceIntentHandler = require(config.SERVICE_INTENT_HANDLER_DIRECTORY);
const registerIntentHandler = require(config.REGISTER_INTENT_HANDLER_DIRECTORY);
const queueStatusIntentHandler = require(config.QUEUE_STATUS_INTENT_HANDLER_DIRECTORY);
const welcomeIntentHandler = require(config.WELCOME_INTENT_HANDLER_DIRECTORY);
const getNameIntentHandler = require(config.GET_NAME_INTENT_HANDLER_DIRECTORY);
const cancelIntentHandler = require(config.CANCEL_INTENT_HANDLER_DIRECTORY);

// these 4 IntentParam are for parameter checking (to prevent trapped prompting)
const doctorIntentParam = require(config.DOCTOR_INTENT_PARAM_DIRECTORY);
const serviceIntentParam = require(config.SERVICE_INTENT_PARAM_DIRECTORY);
const queueStatusIntentParam = require(config.QUEUE_STATUS_INTENT_PARAM_DIRECTORY);
const registerIntentParam = require(config.REGISTER_INTENT_PARAM_DIRECTORY);


const chatHistory = require(config.CHAT_HISTORY_DIRECTORY);
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 8000;
const mongodbClient = require('mongodb').MongoClient;

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

//fallback when no matching parameters
const fallback = () => {
    const responseObj = {
        'fulfillmentText': 'API responded perfectly',
        'fulfillmentMessages': [{
            'text': {
                'text': 'Wah, maaf saya tidak mengerti...'
            }
        }],
        'source': ''
    }
    return responseObj;
}

// client connecting to the db
const client = new mongodbClient(config.DB_URL, {
    useNewUrlParser: true
});

client.connect(function (err, client) {
    console.log("connected");
    // specify which db to use
    const db = client.db(config.DB_NAME);
    // handle parameter and calling appropriate function
    app.post('/', (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const requestObj = req.body;
        const payload = requestObj.originalDetectIntentRequest.payload;
        const text = requestObj.queryResult.queryText;
        console.log('payload =>' + JSON.stringify(payload));
        chatHistory.saveChat(payload, text, db);
        // intent checking to choose which handler will be used
        switch (requestObj.queryResult.intent.displayName) {
            case config.WELCOME_INTENT:
                welcomeIntentHandler.searchName(requestObj.originalDetectIntentRequest.payload, requestObj.session, db, res);
                break;
            case config.GET_NAME_INTENT:
                getNameIntentHandler.register(requestObj.queryResult.queryText, requestObj.originalDetectIntentRequest.payload, requestObj.session, db, res);
                break;
                // doctorIntent and doctorWithSpecializationIntent handled by the same handled and API
            case config.DOCTOR_INTENT:
            case config.DOCTOR_WITH_SPECIALIZATION_INTENT:
                // check whether all required parameters are present
                if (doctorIntentParam.allParamPresent(requestObj.queryResult.intent.displayName, requestObj.queryResult.parameters, requestObj.queryResult.outputContexts) == config.PARAM_FULFILLED) {
                    cancelIntentHandler.reset(config.CANCEL_INTENT_OUTPUT_PHRASE, requestObj.queryResult.outputContexts);
                    return doctorIntentHandler.searchDoctor(requestObj.queryResult.parameters, requestObj.queryResult.outputContexts).then(output => {
                        return res.json(output)
                    });
                }
                // if there is a parameter which is not present, then ask for it
                else {
                    return res.json(doctorIntentParam.allParamPresent(requestObj.queryResult.intent.displayName, requestObj.queryResult.parameters, requestObj.queryResult.outputContexts));
                };

            case config.SERVICE_INTENT:
                if (serviceIntentParam.allParamPresent(requestObj.queryResult.parameters, requestObj.queryResult.outputContexts) == config.PARAM_FULFILLED) {
                    return serviceIntentHandler.searchService(requestObj.queryResult.parameters, requestObj.queryResult.outputContexts).then(output => {
                        return res.json(output);
                    });
                } else {
                    return res.json(serviceIntentParam.allParamPresent(requestObj.queryResult.parameters, requestObj.queryResult.outputContexts));
                }

            case config.QUEUE_STATUS_INTENT:
                if (queueStatusIntentParam.allParamPresent(requestObj.queryResult.queryText, requestObj.queryResult.outputContexts) == config.PARAM_FULFILLED) {
                    return queueStatusIntentHandler.viewQueue(requestObj.queryResult, requestObj.queryResult.outputContexts).then(output => {
                        return res.json(output)
                    });
                } else {
                    return res.json(queueStatusIntentParam.allParamPresent(requestObj.queryResult.queryText, requestObj.queryResult.outputContexts));
                }
            case config.REGISTER_INTENT:
                if (registerIntentParam.allParamPresent(requestObj.queryResult.parameters, requestObj.queryResult.outputContexts) == config.PARAM_FULFILLED) {
                    return registerIntentHandler.consultRegistration(requestObj.queryResult.parameters, requestObj.originalDetectIntentRequest.payload, requestObj.queryResult.outputContexts).then(output => {
                        return res.json(output);
                    });
                } else {
                    return res.json(registerIntentParam.allParamPresent(requestObj.queryResult.parameters, requestObj.queryResult.outputContexts));
                }
                // as for the cancelIntent, reset context to just given-name so the agent can still remember the name of user
            case config.CANCEL_INTENT:
                return res.json(cancelIntentHandler.reset(config.CANCEL_INTENT_OUTPUT_PHRASE, requestObj.queryResult.outputContexts));
                // if no intent matched, send a fallback
            default:
                return res.json(fallback());
        }
    });

    app.listen(port, () => {
        console.log('We are live on ' + port);
    });

});